from stm.preprocess import normalize
from stm.feature.peaks import find_local_peaks, refine_peaks
from skimage.morphology import disk
from scipy.spatial import cKDTree as KDTree
import numpy as np

def precision_recall(predicted, target, sampling):
    """Precision and recall for peak positions"""
    # Precision: Number of correctly predicted peaks 
    # divided by number of target peaks
    distance=0.5/sampling
    if len(predicted) == 0:
        return (0.0, 1.0)
    if len(target) == 0:
        return (1.0, 0.0)
    tree = KDTree(target)
    x = tree.query(predicted, distance_upper_bound=distance)[0]
    precision = (x <= distance).sum() / len(predicted) #Rammer altid 1
    
    # Recall: Number of target peaks that were found
    # divided by total number of target peaks
    tree = KDTree(predicted)
    x = tree.query(target, distance_upper_bound=distance)[0]
    recall = (x <= distance).sum() / len(target) #Rammer altid 0
    return (precision, recall)

def evaluate_result(inference, label, sampling, accept_distance=2.0, threshold=0.6):
    
    #inference is predictions. it has shape (360,360,3)
    "Evaluate the prediction for an image."
    distance = int(accept_distance / sampling)
    image_features = np.size(inference[0,0,:]) - 1 #It is 2, as expected
    
    #inference has shape (1000,360,360,3)
    #Lav et som kan klare Mo og et for S (flere index til output, gør så det kan klare N classes, tilføj et Z koordinat for atom nummer)
    # Find the peaks

    infer_list = []
    label_list = []
    for i in range(image_features):
    
      infer_peaks = find_local_peaks(inference[:,:,i], min_distance=distance, 
                                     threshold=threshold, exclude_border=10,exclude_adjacent=True)
      label_peaks = find_local_peaks(label[:,:,i], min_distance=distance, 
                                     threshold=threshold, exclude_border=10,exclude_adjacent=True)
      # Refine the peaks
      region = disk(2)
      infer_refined = refine_peaks(normalize(inference[:,:,i]), infer_peaks, 
                                  region, model='polynomial')
      #print("infer_refined shape is: ", infer_refined.shape) #Den er tom, når ikke forskudt er den 2xN, hvor værdierne er omkring 10-15,10-300 OBS ser ud til at virke nu
      
      label_refined = refine_peaks(normalize(label[:,:,i]), label_peaks, 
                                  region, model='polynomial')
      #print("refine_peaks shape is: ", label_refined.shape)
      if len(infer_refined):
        temp = np.zeros((len(infer_peaks), 3))
        temp[:,:2] = infer_refined
        temp[:,2] = i * 1000
        infer_list.append(temp)
      if len(label_refined):
        temp = np.zeros((len(label_peaks), 3))
        temp[:,:2] = label_refined
        temp[:,2] = i * 1000
        label_list.append(temp)                           
    (precision, recall) = precision_recall(np.concatenate(infer_list), np.concatenate(label_list), sampling)       
    #print("Precision is: ", precision) #Rammer altid 1
    #print("Recall is: ", recall) #Rammer altid 0
    
    return (precision, recall)





















def precision_recall_original(predicted, target, sampling):
    """Precision and recall for peak positions"""
    # Precision: Number of correctly predicted peaks 
    # divided by number of target peaks
    distance=0.5/sampling
    if len(predicted) == 0:
        return (0.0, 1.0)
    if len(target) == 0:
        return (1.0, 0.0)
    tree = KDTree(target)
    x = tree.query(predicted, distance_upper_bound=distance)[0]
    precision = (x <= distance).sum() / len(predicted)
    # Recall: Number of target peaks that were found
    # divided by total number of target peaks
    tree = KDTree(predicted)
    x = tree.query(target, distance_upper_bound=distance)[0]
    recall = (x <= distance).sum() / len(target)
    return (precision, recall)

def evaluate_result_original(inference, label, sampling, accept_distance=2.0, threshold=0.6):
    "Evaluate the prediction for an image."
    distance = int(accept_distance / sampling)
    
    #Lav et som kan klare Mo og et for S (flere index til output, gør så det kan klare N classes, tilføj et Z koordinat for atom nummer)
    # Find the peaks
    infer_peaks = find_local_peaks(inference[:,:,0], min_distance=distance, 
                                   threshold=threshold, exclude_border=10,
                                   exclude_adjacent=True)
    label_peaks = find_local_peaks(label[:,:,0], min_distance=distance, 
                                   threshold=threshold, exclude_border=10,
                                   exclude_adjacent=True)

    # Refine the peaks
    region = disk(2)
    infer_refined = refine_peaks(normalize(inference[:,:,0]), infer_peaks, 
                                region, model='polynomial')
    label_refined = refine_peaks(normalize(label[:,:,0]), label_peaks, 
                                region, model='polynomial')
    return precision_recall(infer_refined, label_refined, sampling)
